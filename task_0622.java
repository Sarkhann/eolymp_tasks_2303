import java.util.Scanner;
public class task_0622 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        long n = scan.nextLong();
        int m , sum = 0;
        while (n != 0){
            if (n%2 == 1){
                sum++;
            }
            n/=2;
        }
        System.out.println(sum);
    }
}
