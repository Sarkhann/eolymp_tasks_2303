import java.util.Scanner;
public class task_tree {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int k = n;
        for (int i = 0; i < n; i++) {
            for (int m = k - 1; m > 0; m--) {
                System.out.print(" ");
            }
            for (int j = 0; j < i * 2 + 1; j++) {
                System.out.print("*");
            }
            System.out.println();
            k -= 1;
        }
    }
}
