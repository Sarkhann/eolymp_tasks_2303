import java.util.Scanner;

public class task_2862 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        long n = scan.nextLong();
        long sum = 1;
        for (long i = 1; i <= n / 2; i++) {
            if (n % i == 0)
                sum++;

        }
        System.out.println(sum);

    }
}