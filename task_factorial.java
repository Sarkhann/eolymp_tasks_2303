import java.util.Scanner;
public class task_factorial {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        long total = 1;
        for (int i = n; i > 0; i--) {
            total *= i;
        }
        System.out.println(total);
    }
}